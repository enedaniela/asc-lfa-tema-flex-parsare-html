#Daniela Ene 332CC

CC = g++
CFLAGS = -Wall -g 

all: build

parse.cpp: parse.lex
	flex --outfile=$@ $<

build: parse

parse: parse.o 
	$(CC) $(CFLAGS) -o $@ $^ -lfl

.PHONY: clean

run:
	./parse $(arg)
run1: 
	./parse test1.in
run2: 
	./parse test2.in

clean:
	rm -f *.o parse.cpp parse
