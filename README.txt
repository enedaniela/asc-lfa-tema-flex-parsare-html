/*
Ene Daniela Elena 
332CC
*/

Starea initiala a automatului este starea INITIAL in care se asteapta pana se
intalneste caracterul '<' ceea ce seminifica inceputul unui tag si trecerea
in starea START_TAG_NAME, unde se analizeaza daca continutul trebuie ignorat
sau daca avem un tag de inceput sau de sfarsit valid. Din aceasta stare se
trece fie in BUILD_START_TAG_NAME, fie in BUILD_END_TAG_NAME. In starea
BUILD_START_TAG_NAME se construieste numele tagului si se adauga pe stiva,
iar in starea BUILD_END_TAG_NAME se construieste numele tagului si se compara
cu elementul din varful stivei. Starea IGNORE parcurge input-ul, fara a analiza
textul pana la intalnirea caracterului '>'. Starea SINGLE_TAG, acopera situatia
in care avem taguri fara pereche, tag care nu se mai adauga pe stiva.

Probleme aparute: consider ca debugging-ul pentru sintaxa a fost cea mai mare 
problema, deoarece erorile prezentate in consola sunt foarte vagi.

De asemenea mentionez ca programul meu nu trateaza cazurile in care atributele
tagurilor contin stringuri in care se gasesc caracterele '<' si '>' sau cod 
HTML prezentat sub forma de string.

Sistemul de operare sub care s-a realizat tema este Ubuntu Debian 16.0.