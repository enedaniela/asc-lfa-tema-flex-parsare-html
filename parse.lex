/*
Ene Daniela Elena 
332CC
*/

%top{

#include <stdio.h>
#include <iostream>
#include <stack>

#define lastchar yytext[yyleng - 1]
#define YY_BUF_SIZE 66000

std::string start_tag_name;
std::string end_tag_name;
std::stack<std::string> st;
int line = 1;


void overlapping_elements();
void unterminated_elements();
void wrong_tag_name();

}
%s START_TAG_NAME IGNORE BUILD_START_TAG_NAME BUILD_END_TAG_NAME CLOSE_START_TAG CHECK_OVERLAPPING FINAL SINGLE_TAG

symbols "$"|"%"|"&"|"!"|"#"|"-"|"."|":"|";"|"="|"@"|"["|"]"|"^"|"`"|"~"|"_"|"?"|"\""|"'"|"*"|"|"|"+"|" "|"/"|","|"}"|"{"
lowercase [a-z]
uppercase [A-Z]
digits [0-9]
invalid_tag {symbols}|{uppercase}|{digits}
valid_tag {lowercase}
everything {symbols}|{lowercase}|{uppercase}|{digits}

%%
[\t\r] /*skip tabs, line endings*/

<INITIAL>{ 
	"<" BEGIN(START_TAG_NAME);
	"\n" {
		line++;
		BEGIN(INITIAL);
	}
	{everything} BEGIN(INITIAL);
}

<START_TAG_NAME>{
	"?" BEGIN(IGNORE);
	"!" BEGIN(IGNORE);
	{valid_tag} {
		start_tag_name.push_back(lastchar);
		BEGIN(BUILD_START_TAG_NAME);
	}
	"/" BEGIN(BUILD_END_TAG_NAME);
	"\n" {
		line++;
		BEGIN(START_TAG_NAME);
	}
	{invalid_tag} wrong_tag_name();	
}

<IGNORE>{
	">" BEGIN(INITIAL);
	"\n" {
		line++;
		BEGIN(IGNORE);
	}
	{everything} BEGIN(IGNORE);
}

<BUILD_START_TAG_NAME>{
	"/" BEGIN(SINGLE_TAG);
	">" {
		st.push(start_tag_name);
		start_tag_name = "";
		BEGIN(INITIAL);
	}
	" " BEGIN(CLOSE_START_TAG);
	"\n" {
		line++;
		BEGIN(BUILD_START_TAG_NAME);
	}
	{valid_tag} {
		start_tag_name.push_back(lastchar);
		BEGIN(BUILD_START_TAG_NAME);
	}
	
	{invalid_tag} wrong_tag_name();	
}

<SINGLE_TAG>{
	">" {
		BEGIN(INITIAL);
		start_tag_name = "";
	}
	"\n" {
		line++;
		BEGIN(SINGLE_TAG);
	}
	{everything} wrong_tag_name();
}

<CLOSE_START_TAG>{
	">" {
		st.push(start_tag_name);
		start_tag_name = "";
		BEGIN(INITIAL);
	}
	"\n" {
		line++;
		BEGIN(CLOSE_START_TAG);
	}
	{everything} BEGIN(CLOSE_START_TAG);
}

<BUILD_END_TAG_NAME>{
	{valid_tag} {
		end_tag_name.push_back(lastchar);
		BEGIN(BUILD_END_TAG_NAME);
	}
	">" {
		std::string top_elem;
		top_elem = st.top();
		st.pop();
		if(top_elem.compare(end_tag_name) != 0)
			overlapping_elements();
		else{
			end_tag_name = "";
			BEGIN(INITIAL);
		}
	}
	"\n" {
		line++;
		BEGIN(BUILD_END_TAG_NAME);
	}
	{invalid_tag} wrong_tag_name();
}
%%

void overlapping_elements() {
	printf("Fisierul este incorect. Imbricare gresita. Eroare la linia %d\n", line);
	exit(0);
}


void unterminated_elements() {
	printf("Fisierul este incorect. Nu se inchid toate tagurile. Eroare la linia %d\n", line);
	exit(0);
}

void wrong_tag_name() {
	printf("Fisierul este incorect. Numele tagului este incorect. Eroare la linia %d\n", line);
	exit(0);
}

/*used for debugging*/
void print_stack(){

	std::string top_elem;
	while(!st.empty())
    {
	    top_elem = st.top();
	    printf("%s\n",top_elem.c_str());
	    st.pop();
	}
}

int main( int argc, char *argv[] ) 
{

	if(argc < 2)
		printf("Numarul de argumente este gresit\n");
    FILE* f = fopen(argv[1],"r");

    yyrestart(f);
    yylex();

    if(!st.empty())
    	unterminated_elements();
    else
    	printf("Fisierul este corect.\n");

    fclose(f);
}